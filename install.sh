#!/bin/bash

#
#                                                                    _          _ _ 
# __      _____ _ __ ___        ___  ___ _ ____   _____ _ __      ___| |__   ___| | |
# \ \ /\ / / __| '_ ` _ \ _____/ __|/ _ \ '__\ \ / / _ \ '__|____/ __| '_ \ / _ \ | |
#  \ V  V /\__ \ | | | | |_____\__ \  __/ |   \ V /  __/ | |_____\__ \ | | |  __/ | |
#   \_/\_/ |___/_| |_| |_|     |___/\___|_|    \_/ \___|_|       |___/_| |_|\___|_|_|
#
#

# Dependency : https://github.com/ryanoasis/nerd-fonts

# === === ===
# VARIABLES
# === === ===

DISTRO=
DISTRO_INDEX=
BIN_LIST=("zsh" "vim")
BIN_DIR="/bin /usr/bin"
OS_LIST=("archlinux" 
	"centos" 
	"fedora" 
	"raspbian"
	"debian")
OS_INSTALL=("pacman -S"
	"dnf install"
	"apt install"
	"apt install")
OS_LOGO=(" "
	" "
	" "
	" "
	" ")
OS_COLOR=("069"
	"214"
	"198"
	"198")
DEFAULT_COLOR_FG="255"
DEFAULT_COLOR_BG_ROOT="196"
OS_RELEASE=/etc/os-release
INSTALL_DIR=$(pwd)
COLOR_LIST=("")

# === === ===
# FUNCTIONS
# === === ===

usage() {
	echo "USAGE : ${1}"
	exit 1;
}

logger() {
	echo " --[ ${1}"
}

package_install() {
	yes | ${OS_INSTALL[${DISTRO_INDEX}]} ${1}
}

# === === ===
# MAIN
# === === ===

# Arguments-checker
if [ ${#} -gt 0 ]; then
	usage "${0} does not provide argument support for now.
	Please use : ${0}"
fi

# Root-checker
if [ $(whoami) != "root" ]; then
	usage "${0} must be run as root"
fi

# Distro-finder
i=0
for distro in ${OS_LIST[*]}; do
	grep -i ${distro} ${OS_RELEASE} &>/dev/null
	if [ ${?} -eq 0 ]; then
		DISTRO=${distro}
		DISTRO_INDEX=i
		break
	fi
	i=${i}+1
done

# Distro-checker
if [ -n "$DISTRO" ]; then
	logger "Distribution found : ${DISTRO}"
else
	usage "No distro found in the database.
	Please check ${OS_RELEASE} content or add a distro entry in ${0}"
fi

# Binaries-installer
for bin in ${BIN_LIST[*]}; do
	find ${BIN_DIR} -name ${bin} | grep ${bin} &>/dev/null
	if [ ${?} -eq 0 ]; then
		logger "${bin} is already installed. Doing nothing"
	else
		logger "${bin} is not installed. Installing it for you"
		package_install ${bin}
	fi
done

# Configs-installer
logger "Installing zshrc config"
ln -sfn ${INSTALL_DIR}/config/zshrc /etc/zsh/
ln -sfn ${INSTALL_DIR}/config/zshrc /etc/zshrc
# Setting-up distro settings
sed -i "1s@.*@OS_LOGO=${OS_LOGO[DISTRO_INDEX]}@" ${INSTALL_DIR}/config/zshrc
sed -i "2s@.*@COLOR_FG=${DEFAULT_COLOR_FG}@" ${INSTALL_DIR}/config/zshrc
sed -i "3s@.*@COLOR_BG=${OS_COLOR[DISTRO_INDEX]}@" ${INSTALL_DIR}/config/zshrc
sed -i "4s@.*@COLOR_BG_ROOT=${DEFAULT_COLOR_BG_ROOT}@" ${INSTALL_DIR}/config/zshrc

